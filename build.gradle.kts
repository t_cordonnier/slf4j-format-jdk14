import java.io.File
import java.io.FileInputStream
import java.util.Properties

plugins {
    groovy
    signing
    `java-library`
    `java-library-distribution`
    `maven-publish`
    id("com.github.spotbugs") version "5.1.3"
    id("com.diffplug.spotless") version "6.15.0"
    id("com.palantir.git-version") version "0.13.0" apply false
    id("io.github.gradle-nexus.publish-plugin") version "2.0.0-rc-1"
}

fun getProps(f: File): Properties {
    val props = Properties()
    try {
        props.load(FileInputStream(f))
    } catch (t: Throwable) {
        println("Can't read $f: $t, assuming empty")
    }
    return props
}

// we handle cases without .git directory
val home = System.getProperty("user.home")
val javaHome = System.getProperty("java.home")
val props = project.file("src/main/resources/version.properties")
val dotgit = project.file(".git")
if (dotgit.exists()) {
    apply(plugin = "com.palantir.git-version")
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = when {
        details.isCleanTag -> baseVersion
        else -> baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }
    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
                pom {
                    name.set("slf4j-format-jdk14")
                    description.set("JUL style format decorator for SLF4J")
                    url.set("https://codeberg.org/miurahr/slf4j-format-jdk14")
                    licenses {
                        license {
                            name.set("The GNU General Public License, Version 3")
                            url.set("https://www.gnu.org/licenses/licenses/gpl-3.html")
                            distribution.set("repo")
                        }
                    }
                    developers {
                        developer {
                            id.set("miurahr")
                            name.set("Hiroshi Miura")
                            email.set("miurahr@linux.com")
                        }
                    }
                    scm {
                        connection.set("scm:git:git://codeberg.org/miurahr/slf4j-format-jdk14.git")
                        developerConnection.set("scm:git:git://codeberg.org/miurahr/slf4j-format-jdk14.git")
                        url.set("https://codeberg.org/miurahr/sld4j-foramt-jdk14")
                    }
                }
            }
        }
    }

    val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
    tasks.withType<Sign> {
        onlyIf { details.isCleanTag && (signKey != null) }
    }

    signing {
        when (signKey) {
            "signingKey" -> {
                val signingKey: String? by project
                val signingPassword: String? by project
                useInMemoryPgpKeys(signingKey, signingPassword)
            }
            "signing.keyId" -> {/* do nothing */}
            "signing.gnupg.keyName" -> {
                useGpgCmd()
            }
        }
        sign(publishing.publications["mavenJava"])
    }
    nexusPublishing {
        repositories {
            sonatype()
        }
    }
} else if (props.exists()) { // when version.properties already exist, just use it.
    version = getProps(props).getProperty("version")
}

tasks.register("writeVersionFile") {
    val folder = project.file("src/main/resources")
    if (!folder.exists()) {
        folder.mkdirs()
    }
    props.delete()
    props.appendText("version=" + project.version)
}

tasks.getByName("jar") {
    dependsOn("writeVersionFile")
}

group = "tokyo.northside"

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
    withSourcesJar()
    withJavadocJar()
}

dependencies {
    implementation("org.slf4j:slf4j-api:2.0.7")
    testImplementation("org.apache.groovy:groovy-all:4.0.3")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation("ch.qos.logback:logback-classic:1.4.11")
}

tasks.spotbugsMain {
    excludeFilter.set(project.file("config/spotbugs/exclude.xml"))
    reports.create("html") {
        required.set(true)
        outputLocation.set(file("$buildDir/reports/spotbugs.html"))
        setStylesheet("fancy-hist.xsl")
    }
}

tasks.spotbugsTest {
    enabled = false
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}
