# slf4j-format-jdk14

Decorator for SLF4J format extension to support `java.util.logging` style message format and
localization with a resource bundle.

## Status

Currently under active development and considered ALPHA status.

## How to use

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tokyo.northside.logging.LoggerDecorator;

class Main {
    public static void example() {
        ResourceBundle bundle = ResourceBundle.getBundle("org/example/Bundle");
        try {
            Logger logger = LoggerFactory.getLogger(Logger.GLOBAL_LOGGER_NAME);
            new LoggerDecorator(logger.atInfo()).log("JUL style format {0}", "argument");
        } catch (Exception ex) {
            // get format string from bundle
            new LoggerDecorator(logger.atError(), bundle).setMessage("ERROR_MESSAGEKEY", bundle).setCuase(ex).log();
        }
    }
}
```

## Usge with helper

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tokyo.northside.logging.LoggerDecorator;

public final class Util {
    private Util() {
    }
    
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("Bundle");
    
    public static LoggerDecorator deco(LoggingEventBuilder loggingEventBuilder) {
        return new LoggerDecorator(loggingEventBuilder, BUNDLE);
    }
}

class Example {
    public static void example() {
        try {
            Logger logger = LoggerFactory.getLogger(Logger.GLOBAL_LOGGER_NAME);
            Util.deco(logger.atInfo()).log("JUL style format {0}", "argument");
        } catch (Exception ex) {
            Util.deco(logger.atError()).setMessageRB("ERROR_MESSAGEKEY").setCuase(ex).log();
        }
    }
}
```

## Log message localization

You can use `setMessageRB(key)` method to specify a key of the specified resource bundle.
A log message is going to be like "INFO: Localized message 'arg' (MSG)" in the following example.

Bundle.properties

```properties
MSG=Localized message '{0}'
```

You can use styles both `SetMessageRB` and `logRB`. Both methods recognize passed string as
a bundle key instead of a message template.

```java
class Example {
    public static void example() {
        Logger logger = LoggerFactory.getLogger(Logger.GLOBAL_LOGGER_NAME);
        Util.deco(logger.atInfo()).setMessageRB("MSG").addArgument("arg").log();
        Util.deco(logger.atInfo()).logRB("MSG", "arg");
    }
}
```

## License

A library is distributed under GNU General public license version 3.0 or later.
