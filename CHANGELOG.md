# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]

## v0.2.3
* Fix CI/CD release configuration

## v0.2.0
* Publish to OSSRH
* CI/CD on ci.codeerg.org

## v0.1.0
* First internal release

[Unreleased]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.2.3...HEAD
[v0.2.3]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.2.0...v0.2.3
[v0.2.0]: https://codeberg.org/miurahr/slf4j-format-jdk14/compare/v0.1.0...v0.2.0
