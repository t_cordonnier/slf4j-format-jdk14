/*
 *  Copyright (C) 2023 Hiroshi Miura, Thomas Cordonnier
 *                2015 Aaron Madlon-Kay, OmegaT team
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.logging;

import java.util.List;

import java.util.function.Supplier;

import org.slf4j.Marker;

/**
 * Decorator for LoggingEventBuilders which do nothing
 * @author Thomas Cordonnier
 */
class LoggerDecoratorPassive extends LoggerDecorator {
    // Not strictly necessary to have a singleton but can save memory
    public final static LoggerDecoratorPassive INSTANCE = new LoggerDecoratorPassive();

    /**
     * {@inheritDoc}
     */
    public LoggerDecorator setMessage(String message) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public LoggerDecorator setMessageRB(String key) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("EI_EXPOSE_REP2")
    @Override
    public LoggerDecorator setCause(Throwable cause) {
        return this;
    }

    @Override
    public LoggerDecorator addMarker(final Marker marker) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggerDecorator addArgument(Object p) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggerDecorator addArgument(Supplier<?> objectSupplier) {
        return this;
    }

    @Override
    public LoggerDecorator addKeyValue(final String key, final Object value) {
        return this;
    }

    @Override
    public LoggerDecorator addKeyValue(final String key, final Supplier<Object> valueSupplier) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggerDecorator setMessage(final Supplier<String> messageSupplier) {
        return this;
    }

    protected LoggerDecorator addArguments(Object... parameters) {
        return this;
    }

    /**
     * publish log message.
     */
    @Override
    public void log() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object arg) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object arg0, final Object arg1) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object... args) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final Supplier<String> messageSupplier) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object arg) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object arg0, Object arg1) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object... args) {
    }

    protected List<Object> getNonNullArguments() {
        return null;
    }
    
}
