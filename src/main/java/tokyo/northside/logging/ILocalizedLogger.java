/*
 *  Copyright (C) 2023 Hiroshi Miura
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.logging;

/**
 * Decorator extension for SLF4J LoggingEventBuilder.
 * @author Hiroshi Miura
 */
public interface ILocalizedLogger {

    /**
     * Set Message from a key of a resource bundle.
     *
     * @param key
     *            message key in a resource bundle.
     * @return this
     */
    ILocalizedLogger setMessageRB(String key);

    /**
     * Equivalent to calling setMessageRB(String) and then log().
     * @param key
     *            message key in a resource bundle.
     */
    void logRB(String key);

    /**
     * Equivalent to calling setMessageRB(String) followed by
     * addArgument(Object) and then log().
     * @param key
     *            message key in a resource bundle.
     * @param arg
     *            first argument to be used with the message to log
     */
    void logRB(String key, Object arg);

    /**
     * Equivalent to calling setMessageRB(String) followed by two calls of
     * addArgument(Object) and then log().
     * @param key
     *            message key in a resource bundle.
     * @param arg0
     *            first argument to be used with the message to log
     * @param arg1
     *            second argument to be used with the message to log
     */
    void logRB(String key, Object arg0, Object arg1);

    /**
     * Equivalent to calling setMessage(String) followed by zero or more calls
     * to addArgument(Object).
     * (depending on the size of args array) and then log().
     * @param key
     *            message key in a resource bundle.
     * @param args
     *            a list (actually an array) of arguments to be used with the message to log
     */
    void logRB(String key, Object... args);
}
