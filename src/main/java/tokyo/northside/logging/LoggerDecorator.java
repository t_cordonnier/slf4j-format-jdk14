/*
 *  Copyright (C) 2023 Hiroshi Miura, Thomas Cordonnier
 *                2015 Aaron Madlon-Kay, OmegaT team
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.logging;

import java.util.Arrays;
import java.util.List;

import org.slf4j.spi.LoggingEventBuilder;
import org.slf4j.spi.NOPLoggingEventBuilder;

import java.util.ResourceBundle;

/**
 * @author Hiroshi Miura
 * @author Thomas Cordonnier
 */
public abstract class LoggerDecorator implements ILocalizedLogger, LoggingEventBuilder {

    /**
     * Stream style logger decorator implementation, which accepts JUL style
     * format based on SLF4J.
     * 
     * @param builder
     *            SLF4J LoggingEventBuilder which return by atLevel(level) or
     *            its variants.
     */
    public static LoggerDecorator deco(LoggingEventBuilder builder) {
        if (builder instanceof NOPLoggingEventBuilder) {
            return LoggerDecoratorPassive.INSTANCE;
        } else {
            // For the moment, create a new one. We can later use a cache if necessary
            return new LoggerDecoratorActive(builder);
        }
    }

    public static LoggerDecorator deco(LoggingEventBuilder builder, ResourceBundle bundle) {
        if (builder instanceof NOPLoggingEventBuilder) {
            return LoggerDecoratorPassive.INSTANCE;
        } else {
            // For the moment, create a new one. We can later use a cache if necessary
            return new LoggerDecoratorActive(builder,bundle);
        }
    }
    
    protected LoggerDecorator addArguments(Object... parameters) {
        getNonNullArguments().addAll(Arrays.asList(parameters));
        return this;
    }
    
    protected abstract List<Object> getNonNullArguments();
    
}
