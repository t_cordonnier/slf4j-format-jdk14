/*
 *  Copyright (C) 2023 Hiroshi Miura, Thomas Cordonnier
 *                2015 Aaron Madlon-Kay, OmegaT team
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  It is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tokyo.northside.logging;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Supplier;

import org.slf4j.Marker;
import org.slf4j.event.KeyValuePair;
import org.slf4j.spi.LoggingEventBuilder;

/**
 * Decorator for event builders which really do something
 * @author Hiroshi Miura
 */
class LoggerDecoratorActive extends LoggerDecorator {
    private String message;
    private ResourceBundle bundle;
    private String key;
    private Supplier<String> messageSupplier;
    private Throwable exception;
    private List<Object> arguments;
    private List<Marker> markers;
    private List<KeyValuePair> keyValuePairs;

    private final LoggingEventBuilder loggingEventBuilder;

    /**
     * Stream style logger decorator implementation, which accepts JUL style
     * format based on SLF4J.
     * 
     * @param builder
     *            SLF4J LoggingEventBuilder which return by atLevel(level) or
     *            its variants.
     */
    public LoggerDecoratorActive(LoggingEventBuilder builder) {
        loggingEventBuilder = builder;
    }

    public LoggerDecoratorActive(LoggingEventBuilder builder, ResourceBundle bundle) {
        this(builder);
        this.bundle = bundle;
    }

    /**
     * {@inheritDoc}
     */
    public LoggerDecorator setMessage(String message) {
        this.message = message;
        this.key = null;
        this.messageSupplier = null;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public LoggerDecorator setMessageRB(String key) {
        this.key = key;
        this.messageSupplier = null;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("EI_EXPOSE_REP2")
    @Override
    public LoggerDecorator setCause(Throwable cause) {
        exception = cause;
        return this;
    }

    @Override
    public LoggerDecorator addMarker(final Marker marker) {
        if (markers == null) {
            markers = new ArrayList<>(2);
        }
        markers.add(marker);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggerDecorator addArgument(Object p) {
        getNonNullArguments().add(p);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggerDecorator addArgument(Supplier<?> objectSupplier) {
        addArgument(objectSupplier.get());
        return this;
    }

    @Override
    public LoggerDecorator addKeyValue(final String key, final Object value) {
        getNonnullKeyValuePairs().add(new KeyValuePair(key, value));
        return this;
    }

    @Override
    public LoggerDecorator addKeyValue(final String key, final Supplier<Object> valueSupplier) {
        getNonnullKeyValuePairs().add(new KeyValuePair(key, valueSupplier));
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LoggerDecorator setMessage(final Supplier<String> messageSupplier) {
        this.messageSupplier = messageSupplier;
        this.key = null;
        return this;
    }

    /**
     * publish log message.
     */
    @Override
    public void log() {
        LoggingEventBuilder tmp = loggingEventBuilder.setMessage(this::getMessage);
        if (exception != null) {
            tmp = tmp.setCause(exception);
        }
        if (markers != null) {
            for (Marker m: markers) {
                tmp = tmp.addMarker(m);
            }
        }
        if (keyValuePairs != null) {
            for (KeyValuePair p: keyValuePairs) {
                tmp = tmp.addKeyValue(p.key, p.value);
            }
        }
        tmp.log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message) {
        setMessage(message).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object arg) {
        setMessage(message).addArgument(arg).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object arg0, final Object arg1) {
        setMessage(message).addArgument(arg0).addArgument(arg1).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final String message, final Object... args) {
        setMessage(message).addArguments(args).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(final Supplier<String> messageSupplier) {
        setMessage(messageSupplier).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key) {
        setMessageRB(key).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object arg) {
        setMessageRB(key).addArgument(arg).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object arg0, Object arg1) {
        setMessageRB(key).addArgument(arg0).addArgument(arg1).log();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logRB(String key, Object... args) {
        setMessageRB(key).addArguments(args).log();
    }

    protected List<Object> getNonNullArguments() {
        if (arguments == null) {
            arguments = new ArrayList<>(3);
        }
        return arguments;
    }

    private List<KeyValuePair> getNonnullKeyValuePairs() {
        if (keyValuePairs == null) {
            keyValuePairs = new ArrayList<>(4);
        }
        return keyValuePairs;
    }

    private Object[] getArgumentArray() {
        if (arguments == null) {
            return null;
        }
        return arguments.toArray();
    }

    /**
     * Formats UI strings.
     * <p>
     * Note: This is only a first attempt at putting right what goes wrong in
     * MessageFormat. Currently it only duplicates single quotes, but it doesn't
     * even test if the string contains parameters (numbers in curly braces),
     * and it doesn't allow for string containg already escaped quotes.
     *
     * @param str
     *            The string to format
     * @param arguments
     *            Arguments to use in formatting the string
     *
     * @return The formatted string
     */
    private static String format(String str, Object... arguments) {
        // MessageFormat.format expects single quotes to be escaped
        // by duplicating them, otherwise the string will not be formatted
        str = str.replaceAll("'", "''");
        return MessageFormat.format(str, arguments);
    }

    private String getMessage() {
        String result;
        if (key != null && bundle != null) {
            result = format(bundle.getString(key), getArgumentArray()) + " (" + key + ")";
        } else if (messageSupplier != null) {
            result = format(messageSupplier.get(), getArgumentArray());
        } else if (message != null) {
            result = format(message, getArgumentArray());
        } else {
            result = "";
        }
        return result;
    }
}
