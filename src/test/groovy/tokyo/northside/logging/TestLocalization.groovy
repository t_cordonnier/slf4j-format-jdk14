package tokyo.northside.logging

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.LoggerContext
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.function.Supplier

class TestLocalization {

    MemoryAppender memoryAppender
    Logger logger = LoggerFactory.getLogger(TestLocalization.class)

    @BeforeEach
    void setup() {
        memoryAppender = new MemoryAppender()
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory())
        logger.addAppender(memoryAppender)
        memoryAppender.start()
    }

    @Test
    void testGeneralUse() {
        logger.atInfo().log("Simple default template '{}'", "info")
        Assertions.assertTrue(memoryAppender.contains("Simple default template 'info'", Level.INFO))
    }

    @Test
    void testDecorator() {
        LoggerDecorator.deco(logger.atInfo()).log("Simple JUL style template {0}", "arg")
        Assertions.assertTrue(memoryAppender.contains("Simple JUL style template arg", Level.INFO))
    }

    @Test
    void testBundle() {
        LogUtil.deco(logger.atWarn()).setMessageRB("MSG").addArgument("utility").addArgument("LogUtil").log()
        Assertions.assertEquals(1, memoryAppender.countEventsForLogger(TestLocalization.class.getName()))
        Assertions.assertTrue(memoryAppender.contains("JUL style template with utility class LogUtil", Level.WARN))
        Assertions.assertTrue(memoryAppender.contains("(MSG)", Level.WARN))
    }

    @Test
    void testProvider() {
        LogUtil.deco(logger.atWarn()).setMessageRB("MSG").addArgument("utility").addArgument(() -> getFqcn(LogUtil.class)).log()
        Assertions.assertTrue(memoryAppender.contains("JUL style template with utility class " + LogUtil.class.getName(), Level.WARN))
    }

    @Test
    void testLogRB() {
        LogUtil.deco(logger.atError()).logRB("NO_PLACEHOLDER")
        LogUtil.deco(logger.atWarn()).logRB("MSG", "test", "TestLocalization")
        LogUtil.deco(logger.atInfo()).logRB("MANY_PLACEHOLDERS", "many", "number of placeholder is", "3")
        Assertions.assertTrue(memoryAppender.contains("Here is no placeholder", Level.ERROR))
        Assertions.assertTrue(memoryAppender.contains("JUL style template with test class TestLocalization", Level.WARN))
        Assertions.assertTrue(memoryAppender.contains("There are many placeholders: number of placeholder is 3", Level.INFO))
    }


    static String getFqcn(Class<?> clazz) {
        return clazz.getName()
    }
    
    class SupplierWithBool extends Supplier<Object> {
        public boolean wasCalled = false;
            
        public Object get() {
            wasCalled = true;
            return "sample";
        }
    }
        
    /** Test that supplier is indeed called when log is not generated **/
    @Test
    void testNullLogger() {
        ((ch.qos.logback.classic.Logger) logger).setLevel(ch.qos.logback.classic.Level.INFO);  // set isDebugEnabled = false 
        SupplierWithBool s = new SupplierWithBool();
        LoggerDecorator ld = LoggerDecorator.deco(logger.atDebug())
        ld.setMessage("Test {0}")
        ld.addArgument(s)
        ld.log()
        Assertions.assertFalse(s.wasCalled);
    }

}
