package tokyo.northside.logging

import org.slf4j.spi.LoggingEventBuilder

class LogUtil {
    static ResourceBundle BUNDLE = ResourceBundle.getBundle("ResourceBundle")

    static LoggerDecorator deco(LoggingEventBuilder loggingEventBuilder) {
        return LoggerDecorator.deco(loggingEventBuilder, BUNDLE);
    }
}
